# INSTALLATION PLAY! 2
/!\ Ce tutoriel a été réalisé sur Ubuntu mais ça doit marcher sur toutes
les autres plateformes à quelques détails près que je préciserai en cas de besoin

Avant tout nous avons besoin de télécharger Play! 2

L'installation de Play! 2 est très simple, il faut juste télécharger la 
version recente de Play! 2 et ensuite ajouter le dossier bin dans votre `PATH`

Play! 2 est un framework basé sur Java ce qui veut dire qu'il ne peut pas
marcher sans Java 8 déjà installé.

Verifier si java est installé en tapant la commande

`java -version`

Si vous obtenez une erreur alors java n'est pas installé donc passé à l'étape

Sinon vous verrez quelque chose comme ça

`openjdk version "1.8.x"`

## Installation Java

Il faut télécharger la version 8 de Java adapter à votre système sur le lien ci-dessous
[Java SE Development Kit 8 Downloads](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

Installer Java en suivant les instructions en fonction de votre plateforme.

Double-cliquer sur le fichier télécharger et suivre les instructions.

Une fois l'installation terminée retaper la commande suivante pour vérifier l'installation.

`java -version`

Vous devez voir le resultat suivant avec d'autres messages : 

`openjdk version "1.8.x"`

On peut passer maintenant à l'installation de Play! 2.

## Installation Play! 2 offline
L'installation par defaut de Play! 2 recherche ses dépendences à chaque lancement ce qui demande à être 
constamment connecté à Internet.

Nous allons installé Play! 2 en mode offline.

[Play! 2 (Offline Distribution)](https://downloads.typesafe.com/typesafe-activator/1.3.10/typesafe-activator-1.3.10.zip)

Ca va prendre du temps si le débit de votre internet n'est pas grand !

Avec ma connexion (658.47M  1.16MB/s in 7m 26s) pour une connexion de 128KB il faudra 9 fois plus de temps.

Double-cliquer sur le fichier zip pour décompresser.

Après décompression nous obtenons un dossier de même nom que le fichier zip sans l'extension '.zip'

Ajoutons maintenant Play! dans notre `PATH`

Pour Windows suivre ce lien [Configuration ou modification de la variable système PATH](https://www.java.com/fr/download/help/path.xml) L'exemple est donné avec Java mais ça marche avec n'importe quel programme.

Pour les systèmes UNIX:

Ajoutez la ligne suivante dans votre `~/.profile` ou `.bash_profile`

`export PATH=$PATH:/path/to/play/bin`

`source ~/.profile`

Vérifions notre installation de Play!

Tapez la commande suivante dans un terminal

`activator`

Si vous obtenez un message comme ça :

`activator: command not found`

Ce que votre installation n'a pas marché

# Projet Sougou
Dans ce projet on va créer un site de vente de trois produits différents.

- Des vêtements

- Des téléphones

- Des bijoux

Chaque produit est gérer par un vendeur

Les acheteurs peuvent acheter dans une seule boutique lors d'un achat mais ils peuvent acheter autant de fois qu'ils veulent.

Nous donnerons plus de détails dans la suite du projet.

## Création du projet
Maintenant qu'on a notre Play! installé on va pouvoir commencer notre application.

Pour créer une nouvelle application Play! il faut executer la commande suivante :

`activator new nom-app play-java`

Ici `nom-app` sera Sougou

play-java veut dire qu'on veut utiliser le template Java de Play!

Si on ne precise pas ce paramêtre on rentre dans le mode interactif de Play! qui nous demandera ce que l'on veut choisir.

`activator new Sougou`
```
Fetching the latest list of templates...

Browse the list of templates: http://lightbend.com/activator/templates
Choose from these featured templates or enter a template name:
  1) minimal-akka-java-seed
  2) minimal-akka-scala-seed
  3) minimal-java
  4) minimal-scala
  5) play-java
  6) play-scala
(hit tab to see a list of all templates)
> 5
```

Notre application est maintenant créer.

`cd Sougou`
`activator run`

Le programme va télécharger des dépendences qu'il n'a pas dans son cache donc pour un permier lancement il faut être connecté à internet.

Quand vous verrez

`--- (Running the application, auto-reloading is enabled) ---`

Ça veut dire que l'application est bien lancée on peut acceder à notre application à l'adresse: [http://localhost:9000](http://localhost:9000)

Voilà ! notre application est bien lancée.

## Les composantes de l'application Play!
Dans cette section on va parler des composentes d'une application Play!

### Le dossier app/
Ce dossier contient tous les codes sources de l'application

- controllers
	Les controllers de l'application qui implementent les actions appelées pas les routes

- views
	Les templates qui contiennent les fichiers *.scala.html, les vues

- models
	Par defaut ce dossier n'est pas créé lors de la création de l'application.

	Nous allons l'ajouter à la main. Ce package va contenir tous nos models


Dans le dossier app/ on va s'interesser uniquement à ces trois dossiers présentés.

### Le dossier conf/
Ce dossier contient les fichiers de configuration

- application.conf

	Ce fichier contient les configurations de l'application.

	Les informations sur la base de données

	Les langues

	Et d'autres configurations

- routes

	Ce fichier contient les associations des URL à une action dans un Controller

### Le dossier public/
Ce dossier contient les fichiers statics comme les javascripts, les images, les css.

### Le fichier build.sbt
Ce fichier contient la définition du projet.

On retrouve également la définition des dépendences.

### Le fichier project/plugins.sbt
Ce fichier definit les plugins utilisés dans notre application.

On va meme activé un plugin dans ce fichier dans la suite du tutoriel.

## Controllers
On va jouer un peu avec notre controller `HomeController.java`

Nous avons une seule méthode (action) 

``` 
public Result index() {
	return ok(index.render("Your new application is ready");
}
```

Modifions ce texte et rechargeons notre navigateur pour voir le resultat.

```
...
return ok(index.render("Bienvenue dans votre application Sougou!");
...
```

Vous vous demandez surement qu'est ce qui fait le lien entre le url localhost:9000 et l'action `index`

Cet lien est créé dans le fichier conf/routes.

conf/routes
```
  1 # Routes
  2 # This file defines all application routes (Higher priority routes first)
  3 # ~~~~
  4
  5 # An example controller showing a sample home page
  6 GET     /                           controllers.HomeController.index
  7 # An example controller showing how to use dependency injection
  8 GET     /count                      controllers.CountController.count
  9 # An example controller showing how to write asynchronous code
 10 GET     /message                    controllers.AsyncController.message
 11
 12 # Map static resources from the /public folder to the /assets URL path
 13 GET     /assets/*file               controllers.Assets.versioned(path="/public", file: Asset)
```
Sur la ligne 6 nous voyons la definition du lien entre la route et l'action

`GET` est la méthode HTTP

`/` Est le chemin (la route)

`controllers.HomeController.index` est le chemin complet de l'action

Nous voyons d'autre definition comme `/message`

Quand on tape `localhost:9000/message` dans le navigateur on appelle l'action `controllers.AsyncController.message du côté serveur.

Pour chaque action écrite dans un controller doit correspondre à une unique ligne dans le fichier conf/routes de la forme:

`GET ou POST   	/definition/de/route  controllers.NomController.action`

Maintenant qu'on a compris comment associer une action à une route ajoutons une action et l'associer à une route

Dans le controller `HomeController` ajouter l'action suivante : 

```
public Result disBonjour(String nom) {
	return ok("Bonjour " + nom);
```

Associons cette action avec la route suivante :

conf/routes

```
...
GET     /                           controllers.HomeController.index
GET	/bonjour/:nom		    controllers.HomeController.disBonjour(nom: String)
...

```

Dans cette definition nous voyons une partie dynamique :nom

C'est comme ça qu'on déclare les variables dynamiques dans les routes

La partie action la definition du paramêtre differt un peu de la definition en Java. On donne le nom du paramêtre deux points suivi du type du paramêtre.

Cliquer ici [Dis Bonjour à Sougou](http:localhost:9000/bonjour/Sougou).

Amusez vous à changer 'Sougou' avec d'autres textes pour voir.

La fonction `ok()` offerte par Play! 2 nous permet de renvoyer des textes et plains d'autres objects Java. Comme dans le cas de l'action `index()` `ok()` a pris en argument un objet de type `Content`.

C'est pourquoi on a pu passer directement un `String`

## Views
On regarder le contenu des views 

### views/index.scala.html

```
  1 @*
  2  * This template takes a single argument, a String containing a
  3  * message to display.
  4  *@
  5 @(message: String)
  6
  7 @*
  8  * Call the `main` template with two arguments. The first
  9  * argument is a `String` with the title of the page, the second
 10  * argument is an `Html` object containing the body of the page.
 11  *@
 12 @main("Welcome to Play") {
 13
 14     @*
 15      * Get an `Html` object by calling the built-in Play welcome
 16      * template and passing a `String` message.
 17      *@
 18     @play20.welcome(message, style = "Java")
 19
 20 }
```

Ligne  5 : Definition des paramêtres de la fonction

Ligne 12 : Appel de la fonction `main()` qui prend deux paramêtres, String et Html

Ligne 18 : Appel de la fonction `play20.welcome(message, style = "Java")`

La fonction `main()` vient du fichier views/main.scala.html

Il faut comprendre que tous les *.scala.html dans le dossier views/ sont compilés en une fonction qu'on peut appeler dans un autre fichier *.scala.html ou côté Java.

Dans l'action `HomeController.index()` c'est justement la fonction générée par le fichier `index.scala.html` qui est appélée avec un String en argument.

### views/main.scala.html

```
  1 @*
  2  * This template is called from the `index` template. This template
  3  * handles the rendering of the page header and body tags. It takes
  4  * two arguments, a `String` for the title of the page and an `Html`
  5  * object to insert into the body of the page.
  6  *@
  7 @(title: String)(content: Html)
  8
  9 <!DOCTYPE html>
 10 <html lang="en">
 11     <head>
 12         @* Here's where we render the page title `String`. *@
 13         <title>@title</title>
 14         <link rel="stylesheet" media="screen" href="@routes.Assets.versioned("stylesheets/main.css")">
 15         <link rel="shortcut icon" type="image/png" href="@routes.Assets.versioned("images/favicon.png")">
 16         <script src="@routes.Assets.versioned("javascripts/hello.js")" type="text/javascript"></script>
 17     </head>
 18     <body>
 19         @* And here's where we render the `Html` object containing
 20          * the page content. *@
 21         @content
 22     </body>
 23 </html>
```

Ligne  7 : Definition des paramêtre de la fonction.

(content: Html) corresponds à la partie entre les {}. Comme dans le fichier index.scala.html ligne 12.

Ligne 21 : On imprime la valeur de `content` à cette place.

L'operateur `@` est l'opérateur magique de Play! 2.

Nous verrons dans les sections à venir comment manipuler les variables dans un template avec Play! 2.

Dans le fichier `views/index.scala.html` modifions la ligne 18 comme suit.

```
...
18     @message
...
```

Rechargeons la page [http://localhost:9000](http://localhost:9000)

C'est normal qu'on ne voit plus les choses magiques qui constituaient notre page index.

On a enlevé le template par defaut de Play! 2.

# Dependences
Dans le fichier build.sbt on va ajouter quelques dependences qu'on va utiliser dans notre application.

## Boostrap & WebJars
Bootstrap est le framework incontournable dans le monde du Web aujourd'huiSi on ne veut pas reinventer la roue.

En plus il y a beaucoup de theme gratuit qu'on pourra utiliser pour demarrer notre application

Tiens, on va utiliser ce theme [Download E Commerce](http://www.bootply.com/download/130150)

Pour voir le theme en ligne suivez ce lien [E-Commerce Template](http://www.bootply.com/render/130150)

Telecharger et décompresser

Deplacer le contenu js/ dans public/javascripts
et de css/ dans public/stylesheets

Et deplacer index.html dans views/
remplacer le contenu de index.scala.html par celui de index.html

Ajouter @(message: String)

Tout en haut du fichier

On va mettre à jour les liens des js et css.

Dans le fichier `views/index.scala.html`

```
...
10                 <link href="@routes.WebJarAssets.at(webJarAssets.locate("css/bootstrap.min.css"))" rel="stylesheet">
11                 <link href="@routes.WebJarAssets.at(webJarAssets.locate("css/font-awesome.min.css"))" rel="stylesheet">             
...
15                 <link href="@routes.Assets.versioned("stylesheets/styles.css")" rel="stylesheet">
...
264                 <script src="@routes.WebJarAssets.at(webJarAssets.locate("jquery.min.js"))"></script>
265                 <script src="@routes.WebJarAssets.at(webJarAssets.locate("js/bootstrap.min.js"))"></script>
...
```
Modifier tous les liens commençant par /assets/example en ajoutant `http://www.bootply.com` devant pour voir la page comme sur le site du theme.
Rechargeons notre page

Nous avons utilisé WebJars pour gerer nos dépendences :

- boostrap

- font-awesome

On a déclaré ces dependences dans le fichier build.sbt

```
15 libraryDependencies ++= Seq(
16   "org.webjars"    %%   "webjars-play"          % "2.5.0-2",
17   "org.webjars"    %    "bootstrap"             % "3.1.1-2",
18   "org.webjars"    %    "font-awesome"          % "4.6.2"
19 )
```
C'est pratique les outils de gestion de dépendences

# Outils de developpement IDE
Il y a beaucoup d'outils qu'on peu utiliser pour developper les applications avec Play!.

Personnellement d'utilise IntelliJ mais il est payant, il y a Eclipse que j'utilise de temps en temps il est gratuit.

Pour configurer votre IDE je vous envoie vers ce site pour plus de détails [Setting up your preferred IDE](https://www.playframework.com/documentation/2.5.x/IDE)


# Models
On va commencer à modeliser nos models maintenant.

Notre client nous a dit qu'il a trois types de produits detenus par trois vendeurs.

Donc nous avons besoin d'une base de données pour stocker les produits et les vendeurs.

On va modifier notre fichier conf/application.conf pour indiquer à l'application la base de données qu'on souhaite utiliser.

Pour cela

Modifier `conf/application.conf` comme suit :

```
## Secret key
# http://www.playframework.com/documentation/latest/ApplicationSecret
# ~~~~~
# The secret key is used to sign Play's session cookie.
# This must be changed for production, but we don't recommend you change it in this file.
play.crypto.secret = "yu;W>pE7Om^8b4m0_Yky6_KU;nMG<^7L`HJ?i^zEv:ZQ@<mU=1BL4FlOJSijG;=c"

## Evolutions
# https://www.playframework.com/documentation/latest/Evolutions
# ~~~~~
# Evolutions allows database scripts to be automatically run on startup in dev mode
# for database migrations. You must enable this by adding to build.sbt:
#
# libraryDependencies += evolutions
#
play.evolutions {
  # You can disable evolutions for a specific datasource if necessary
  #db.default.enabled = false
}

db {
  default.driver = org.h2.Driver
  default.url = "jdbc:h2:mem:play"
  default.username = sa
  default.password = ""
}
```

J'ai enlevé toutes les parties qui ne nous intéressent pas.

Pour commencer nous aller utiliser la base de données H2 qui est une BDD dans la memoire.
C'est très bien pour le developpement.

## Modèle Product.
Nous avons vu que notre client vend des produits donc nous avons besoin 
d'un modèle qui représente les produits.
On le nomme `Product`

Dans notre application ajoutons un package `models` qui contiendra nos modèles.

Pour le moment notre client nous dit qu'un produit a un titre pour le moment.
Donc on va créer le produit avec juste un titre et une image de presentation. Et un identifiant unique 
qui nous permettra d'identifier de façon unique un produit.

### Création de la classe Product.java

```
public class Product {
    public Long id;
    public String title;
    public String presentationImage;
}
```

Notre classe `Product` est maintenant créée.

### Création d'une page pour afficher les produits.
Le client veut que les internautes puissent voir les produits pour pouvoir acheter par la suite.

On va s'inspirer de notre template bootstrap qu'on a télécharger pour afficher nos produits.

Nous allons apporter quelque changement dans notre fichier `index.scala.html` pour afficher notre liste.

On va également faire un peu de factorisation en deplaçant les parties communes 
dans `main.scala.html` ainsi tous les autres fichiers `.scala.html` pourront 
utiliser les parties communes qu'on mettra dans `main.scala.html`

Pour la suite on ne va pas utiliser WebJars pour importer nos fichiers js et css. 
On va le faire à la main. On y reviendra après quand on sera à l'aise avec la base.

Pour recuperer les fichiers js et css télécharger par WebJars rendez vous dans le dossier `target/public/main/lib`
Vous y trouverez les différents fichiers.

Copiez les *.js dans notre `public/javascripts` et les *.css dans `public/stylesheets`

|        Source        |    Destination     |
|          ---         |        ---         |
| font-awesome.min.css | public/stylesheets |
|  bootstrap.min.css   | public/stylesheets |  
|   bootstrap.min.js   | public/javascripts |
|    jquery.min.js     | public/javascripts |
                        
`main.scala.html`
```
@(title: String)(content: Html)

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        @* Here's where we render the page title `String`. *@
        <title>@title</title>
        <link rel="shortcut icon" type="image/png" href="@routes.Assets.versioned("images/favicon.png")">
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link href="@routes.Assets.versioned("stylesheets/bootstrap.min.css")" rel="stylesheet">
        <link href="@routes.Assets.versioned("stylesheets/font-awesome.min.css")" rel="stylesheet">
            <!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
        <link href="@routes.Assets.versioned("stylesheets/styles.css")" rel="stylesheet">
        <link rel="stylesheet" media="screen" href="@routes.Assets.versioned("stylesheets/main.css")">
    </head>
    <body>
            <!--template-->
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="container" style="">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Sell</a>
                </div>
                <div class="collapse navbar-collapse" style="">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#" class="" style="">Explore</a>

                        </li>
                        <li><a href="#" class="">Looks</a>

                        </li>
                        <li><a href="#" class="">Style</a>

                        </li>
                        <li><a href="#" class="">About</a>

                        </li>
                        <li><a href="#myModal" data-toggle="modal" data-target="#myModal">Sign in</a>

                        </li>
                    </ul>
                </div>
                    <!--/.nav-collapse -->
            </div>
        </div>
        @content
            <!-- script references -->
        <script src="@routes.Assets.versioned("javascripts/jquery.min.js")"></script>
        <script src="@routes.Assets.versioned("javascripts/bootstrap.min.js")"></script>
    </body>
</html>
```

`index.scala.html`

```
@(message: String)

@main("Sougou") {
    <div class="container">
        <div class="col-md-12">
            <div class="center-block text-center">
                <h1>E-Commerce Template</h1>
                <p class="lead">Get Your Style On</p>
            </div>
            ...
         </div>
    </div>
    ...
}
```

Une fois tous les fichiers remis à leur place relancer pour s'assurer 
que tout va bien [http://localhost:9000](http://localhost:9000)

On va modifier notre index pour qu'il accepte une liste de produits en paramêtre.

```
@(products: List[Product])
```

Il faut modifier egalement l'action du `HomeController` qui appelle l'index.

```
    public Result index() {
        List<Product> products = new ArrayList<>();
        return ok(index.render(products));
    }
```

Pour revenir à la liste on va toute la partie supérieure dans index et ne 
garder que la partie qui contient la liste. On utilisera la partie supérieure quand on va visualiser un produit.
Donc vous pouvez garder le code dans un fichier.

Après modification de `index.scala.html` On obtient :

```
@(products: List[Product])

@main("Sougou") {
    <div class="container">
    @for(product <- products) {
        <div class="row">
            <div class="col-sm-3">
                <a href="#">
                    <br/>
                    <img class="img-responsive" src="@product.presentationImage" data-alt="" data-title="">
                </a>
                <br>

            </div>
            <div class="col-sm-9">
                <h2><a class="url" href="#"> @product.title</a></h2>
            </div>
        </div>
        <hr>
        }
    </div>
}
```

Si on recharge notre application on verra une page blanche. Ce qui est normal car on en envoyé une liste vide.

Créons quelques produits virtuel pour voir.

Modifier le controller `HomeController` pour ressembler à :

```
...
static ArrayNode productsJson;

    static {
        productsJson = (ArrayNode) Json.parse("[\n" +
                "  {\n" +
                "    \"id\": 1,\n" +
                "    \"title\": \"Ceinture\",\n" +
                "    \"presentationImage\": \"http://www.bootply.com/assets/example/ec_belt.jpg\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 2,\n" +
                "    \"title\": \"Jacket\",\n" +
                "    \"presentationImage\": \"http://www.bootply.com/assets/example/ec_jacket.jpg\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 3,\n" +
                "    \"title\": \"Jeans\",\n" +
                "    \"presentationImage\": \"http://www.bootply.com/assets/example/ec_jeans.jpg\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 4,\n" +
                "    \"title\": \"Chaussettes\",\n" +
                "    \"presentationImage\": \"http://www.bootply.com/assets/example/ec_socks.jpg\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 5,\n" +
                "    \"title\": \"Pull\",\n" +
                "    \"presentationImage\": \"http://www.bootply.com/assets/example/ec_sweater.jpg\"\n" +
                "  }\n" +
                "]");
    }

    public Result index() {
        List<Product> products = new ArrayList<>();
        for (int i = 0; i < productsJson.size(); i++) {
            products.add(Json.fromJson(productsJson.get(i), Product.class));
        }
        return ok(index.render(products));
    }
    ...
```

On recharge à nouveau [http://localhost:9000](http://localhost:9000)

On verra nos produits

## Création d'une veu pour afficher un produit.
Maintenant notre client est content car il arrive à voir la liste des produits qu'il a en sa possession.

Il veut pouvoir afficher les détails d'un produit quand il clique dessus.

Pour faire cela on va modifier notre liste de produits comme suit :

```
...
            <div class="col-sm-3">
                <a href="@routes.HomeController.product(product.id)">
                    <br/>
                    ...
             <div class="col-sm-9">
                 <h2><a class="url" href="@routes.HomeController.product(product.id)"> @product.title</a></h2>
             </div>
        </div>
        ...    
```

On va ensuite Ajouter l'action correspondante dans le controller `HomeController` et la route dans `conf/routes`
 
`HomeController.java`
 
```
public Result product(Long id) {
    return ok(product.render(Json.fromJson(productsJson.get((int) (id - 1) /*L'id commence à 1*/), Product.class)));
}
```
 
`conf/routes`

```
... 
GET     /                           controllers.HomeController.index
GET     /products/$id<[0-9]+>       controllers.HomeController.product(id: Long)
GET     /bonjour/:nom               controllers.HomeController.disBonjour(nom: String)
... 
```
 
Dans la definition de notre route on a utilisé d'autre manière qui utilise 
une expression regulière pour spécifier la nature de notre paramêtre
 
`$id<[0-9]+>` veut dire qu'il faut intercepter unique les id numériques.
 
```
/products/1    ---> OK
/products/ab   ---> KO 
/products/1398 ---> OK
```
 
N'oubliez pas de créer le fichier `product.scala.html` et mettre 
la partie supérieure qu'on a enlevé précédemment.

On recharge à nouveau [http://localhost:9000](http://localhost:9000)

Cliquer sur différents produits pour les afficher.

Nous avons toujours la meme page qui s'affiche.

Modifions le fichier `product.scala.html` pour afficher l'image du produit dans la partie à gauche. Et le titre en bas.

`product.scala.html`

```
...
                    <div class="product col-sm-6">
                        <a href="#"><img class="img-responsive" src="@product.presentationImage"><i class="btn btn-product fa fa-star"></i></a>
                        <hr>
                        <h2>@product.title</h2>
...
```

On recharge à nouveau [http://localhost:9000](http://localhost:9000) et constater le changement.

### Utilisation de la base de données
Notre client veut maintenant que ses vendeurs puissent publier des nouveaux produits.

Pour répondre à ce besoin du grand patron nous allons utiliser une base de données 
qui va enregistrer tous les nouveaux produits publiés par les vendeurs.

Play! 2 offre la possibilité d'utiliser différents outils pour interagir avec la base de données.

- Hibernate

- Ebean

Nous allons utiliser Ebean pour ce tutoriel car il est integrer dans Play! 2.

Pour activer ce plugins nous allons modifier le fichier `project/plugins.sbt` 
en decommentant la ligne correspondant à Ebean

```
...
// Play Ebean support, to enable, uncomment this line, and enable in your build.sbt using
// enablePlugins(PlayEbean).
addSbtPlugin("com.typesafe.sbt" % "sbt-play-ebean" % "1.0.0")
```

Il faut dire à notre application d'utiliser ce plugins en le spécifiant dans le fichier `build.sbt`

```
...
lazy val root = (project in file(".")).enablePlugins(PlayJava).enablePlugins(PlayEbean)
...
```

Il faut modifier le fichier `conf/application.conf` pour spécifier l'endroit où Ebean doit chercher les models.

```
...
ebean.default = "models.*"
```

Il faut relancer activator pour qu'il recharge les nouvelles modifications.

#### Ebean Utilisation
On va modifier nos models pour utiliser Ebean:

- Faire dériver tous les Models de Model

- Annoter la class avec Entity

`Product.java`
```
package models;
import com.avaje.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_PRODUCTS")
public class Product extends Model {
    // Cette variable va nous permettre de passer des requêtes dans la base de données
    public static final Find<Long, Product> find = new Finder<>(Product.class);
    @Id
    @Column(name = "ID") // Definir le nom de la colonne dans la base de donnée.
    public Long id;
    @Column(name = "TITLE")
    public String title;
    // La definition de nom de colonne n'est pas obligatoire.
    // Le nom e l'attribut sera utilisé par defaut.
    public String presentationImage;
}
```

#### Enregistrement dans la base de données
On va modifier notre Controller pour qu'il utilise la base de données.

`HomeController.java`

```
...
    static {
        if(Product.find.findRowCount() == 0) {
            loadData(); // On ajoute les produits lors du chargement de l'application
                        // quand on n'a pas de produit dans la base.
        }
    }
    
    private static void loadData() {
        ArrayNode productsJson = (ArrayNode) Json.parse("[\n" +
                "  {\n" +
                    ...
                "  }\n" +
                "]");
        for (int i = 0; i < productsJson.size(); i++) {
            Product p = Json.fromJson(productsJson.get(i), Product.class);
            p.save();
        }
    }
                    
    public Result index() {
        List<Product> products = Product.find.findList();
        return ok(index.render(products));
    }

    public Result product(Long id) {
        return ok(product.render(Product.find.byId(id)));
    }
...
```

On recharge à nouveau [http://localhost:9000](http://localhost:9000)

On va avoir une error avec un bouton qui nous demande d'appliquer la 
modification apporter à la base de données.

`An SQL script will be run on your database - 'Apply this script now!'`

Cliquer sur le bouton 'Apply this script now!' pour appliquer et après 
nous allons avoir tous nos produits dans la base de données.

## Création d'un formulaire pour poster un produit.
Nous allons pouvoir faire plaisir à notre cher client maintenant.

Créons un fichier `views/newProduct.scala.html` 

```
@import helper._

@(form: Form[Product])

@main("Nouveau produit") {
  <div class="container">
    <form action="@routes.HomeController.createProduct()" method="post">
      @inputText(form("title"))
      <br/>
      @inputText(form("presentationImage"))
      <br/>
      <button class="btn btn-primary">Créer</button>
    </form>
  </div>
}
```

Nous avons utilisé un utilitaire de Play! 2 qui nous permet de créer des 
balises <input> avec les informations du champ passé en paramêtre.

`Form` est aussi un utilitaire fourni par Play! 2 qui symbolise un formulaire côté serveur.
Il permet de valider les champs.

On va modifier notre classe `Product` pour empêcher la création d'un produit sans titre.

```
...
@Column(name = "TITLE")
@Constraints.Required
public String title;
...
```
On ajoute l'annotation `@Constraints.Required` au champ pour indiquer à 
`Form` qu'il doit vérifier que ce champ n'est pas vide lors de la validation.

On crée la fonction pour récupérer les valeurs saisies dans le formulaire lors de l'envoi.
`HomeController.createProduct()`

```
    @Inject
    FormFactory factory; // Instance automatiquement créée.

    public Result createProduct() {
        Form<Product> form = factory.form(Product.class);
        form = form.bindFromRequest();
        if (form.hasErrors()) {
            return badRequest(newProduct.render(form));
        }
        Product product = form.get();
        product.save();
        return redirect(routes.HomeController.product(product.id));
    }
```

Il faut également ajouter la route associée à l'action de création. On profite pour ajouter 
une route l'action associée à l'affichage du formulaire et créer l'action correspondente.

```
GET         /products/new                controllers.HomeController.newProduct()
POST        /products/new                controllers.HomeController.createProduct()
```

`HomeController.newProduct()`
```
public Result newProduct() {
    Form<Product> form = factory.form(Product.class);
    return ok(newProduct.render(form));
}
```