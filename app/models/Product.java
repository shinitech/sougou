package models;
import com.avaje.ebean.Model;
import play.data.validation.Constraints;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by sissoko on 25/05/2016.
 */
@Entity
@Table(name = "T_PRODUCTS")
public class Product extends Model {
    // Cette variable va nous permettre de passer des requêtes dans la base de données
    public static final Find<Long, Product> find = new Finder<>(Product.class);
    @Id
    @Column(name = "ID")
    public Long id;
    @Column(name = "TITLE")
    @Constraints.Required
    public String title;
    @Column(name = "PRESENTATION_IMAGE")
    public String presentationImage;
}
