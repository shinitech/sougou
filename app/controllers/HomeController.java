package controllers;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.inject.Inject;
import models.Product;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.*;

import views.html.*;

import java.util.List;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    static {
        if(Product.find.findRowCount() == 0) {
            loadData(); // On ajoute les produits lors du chargement de l'application
                        // quand on n'a pas de produit dans la base.
        }
    }

    private static void loadData() {
        ArrayNode productsJson = (ArrayNode) Json.parse("[\n" +
                "  {\n" +
                "    \"title\": \"Ceinture\",\n" +
                "    \"presentationImage\": \"http://www.bootply.com/assets/example/ec_belt.jpg\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"title\": \"Jacket\",\n" +
                "    \"presentationImage\": \"http://www.bootply.com/assets/example/ec_jacket.jpg\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"title\": \"Jeans\",\n" +
                "    \"presentationImage\": \"http://www.bootply.com/assets/example/ec_jeans.jpg\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"title\": \"Chaussettes\",\n" +
                "    \"presentationImage\": \"http://www.bootply.com/assets/example/ec_socks.jpg\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"title\": \"Pull\",\n" +
                "    \"presentationImage\": \"http://www.bootply.com/assets/example/ec_sweater.jpg\"\n" +
                "  }\n" +
                "]");
        for (int i = 0; i < productsJson.size(); i++) {
            Product p = Json.fromJson(productsJson.get(i), Product.class);
            p.save();
        }
    }

    public Result index() {
        List<Product> products = Product.find.findList();
        return ok(index.render(products));
    }

    public Result product(Long id) {
        return ok(product.render(Product.find.byId(id)));
    }

    @Inject
    FormFactory factory;

    public Result createProduct() {
        Form<Product> form = factory.form(Product.class);
        form = form.bindFromRequest();
        if (form.hasErrors()) {
            return badRequest(newProduct.render(form));
        }
        Product product = form.get();
        product.save();
        return redirect(routes.HomeController.product(product.id));
    }

    public Result newProduct() {
        Form<Product> form = factory.form(Product.class);
        return ok(newProduct.render(form));
    }

}
