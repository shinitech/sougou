name := """Sougou"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava).enablePlugins(PlayEbean)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs
)

libraryDependencies ++= Seq(
  "org.webjars"    %%   "webjars-play"          % "2.5.0-2",
  "org.webjars"    %    "bootstrap"             % "3.1.1-2",
  "org.webjars"    %    "font-awesome"          % "4.6.2"
)
